import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.css']
})
export class ThemesComponent {
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Buttons', cols: 1, rows: 1 },
          { title: 'Navegation', cols: 1, rows: 1 },
          { title: 'Paginator', cols: 1, rows: 1 },
          { title: 'Especials', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Buttons', cols: 2, rows: 1 },
        { title: 'Navegation', cols: 1, rows: 1 },
        { title: 'Paginator', cols: 1, rows: 2 },
        { title: 'Especials', cols: 1, rows: 1 }
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}
}
